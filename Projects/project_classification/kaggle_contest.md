# Project 3: Kaggle Contest 

In this project you will develop or enhance a model to classify point clouds. A baseline implementation will be given to you. Which will result in 76% accuracy without any bells and whistles. Your task is to achieve at least 83% accuracy or more. 

We have adapted SemanticKITTI dataset and created Class Separated KITTI or ClaSeK dataset. You will run your model on this dataset. In the figure you can see some classes from ClaSeK dataset. 

![ClaSek](ClaSeK.png)


Click on this link: https://www.kaggle.com/t/f2d8f04895ef4c01b05783bf9132dd3b to join the contest. 
Please send me your kaggle username via email. 